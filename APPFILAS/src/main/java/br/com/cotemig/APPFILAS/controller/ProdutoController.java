package br.com.cotemig.APPFILAS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.APPFILAS.model.Produto;
import br.com.cotemig.APPFILAS.service.ProdutoService;

@Controller
public class ProdutoController {
	
	@Autowired
	private ProdutoService produtoService;
	
	@RequestMapping(value = "/produto/insert", method = RequestMethod.GET)
	public ModelAndView insert() {
		return new ModelAndView("insertProduto", "produto", new Produto());
	}
	 
	@RequestMapping(value = "/produto/insert", method = RequestMethod.POST)
	public String submitInsert(@ModelAttribute("produto") Produto produto, BindingResult result, ModelMap model) {
	        
		if (result.hasErrors()) {
			return "error";
		}
		        
		produtoService.insertProduto(produto);
		return "redirect:/produto/index";
	}
	 
	@RequestMapping(value = "/produto/delete", method = RequestMethod.GET)
	public ModelAndView delete(Integer id) {
		return new ModelAndView("deleteProduto", "produto", produtoService.getProdutoById(id).get());
	}
	 
	@RequestMapping(value = "/produto/delete", method = RequestMethod.POST)
	public String submitDelete(@ModelAttribute("produto") Produto produto, BindingResult result, ModelMap model) {
		
		if (result.hasErrors()) {
			return "error";
		}
		 
		produtoService.deleteProdutoById(produto.getId());
		return "redirect:/produto/index";
	}
	
	@RequestMapping(value = "/produto/update", method = RequestMethod.GET)
	public ModelAndView update(Integer id) {
		return new ModelAndView("updateProduto", "produto", produtoService.getProdutoById(id).get());
	}
	
	@RequestMapping(value = "/produto/update", method = RequestMethod.POST)
	public String submitUpdate(@ModelAttribute("produto") Produto produto, BindingResult result, ModelMap model) {
		        
		if (result.hasErrors()) {
			return "error";
		}
		 
		produtoService.updateProduto(produto);
		return "redirect:/produto/index";
	}
	

	@RequestMapping(value = "/produto/index", method = RequestMethod.GET)
	public ModelAndView index() {
	        
	    ModelAndView mav = new ModelAndView("indexProduto");
	    mav.addObject("listaProdutos", produtoService.getAllProduto());
	    return mav;
	 }
	

	@RequestMapping(value = "/produto/read", method = RequestMethod.GET)
	public ModelAndView read() {
	        
	    ModelAndView mav = new ModelAndView("readProduto");
	    mav.addObject("listaProdutos", produtoService.getAllProduto());
	    return mav;
	}
}
