package br.com.cotemig.APPFILAS.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.cotemig.APPFILAS.model.Loja;

@Repository("lojaRepository")
public interface LojaRepository extends JpaRepository<Loja, Integer> {
	
}
