package br.com.cotemig.APPFILAS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.APPFILAS.model.Loja;
import br.com.cotemig.APPFILAS.service.LojaService;

@Controller
public class LojaController {
	
	@Autowired
	private LojaService lojaService;
		
	@RequestMapping(value = "/loja/insert", method = RequestMethod.GET)
	public ModelAndView insert() {
		return new ModelAndView("insertLoja", "loja", new Loja());
	}
	
	@RequestMapping(value = "/loja/insert", method = RequestMethod.POST)
	public String submitInsert(@ModelAttribute("loja") Loja loja, BindingResult result, ModelMap model) {
	        
		if (result.hasErrors()) {
			return "error";
		}
		        
		lojaService.insertProduto(loja);
		return "redirect:/loja/index";
	}
	
	@RequestMapping(value = "/loja/delete", method = RequestMethod.GET)
	public ModelAndView delete(Integer id) {
		return new ModelAndView("deleteLoja", "loja", lojaService.getLojaById(id).get());
	}
	
	@RequestMapping(value = "/loja/delete", method = RequestMethod.POST)
	public String submitDelete(@ModelAttribute("loja") Loja loja, BindingResult result, ModelMap model) {
		
		if (result.hasErrors()) {
			return "error";
		}
		 
		lojaService.deleteLojaById(loja.getId());
		return "redirect:/loja/index";
	}
	
	@RequestMapping(value = "/loja/update", method = RequestMethod.GET)
	public ModelAndView update(Integer id) {
		return new ModelAndView("updateLoja", "loja", lojaService.getLojaById(id).get());
	}
	
	@RequestMapping(value = "/loja/update", method = RequestMethod.POST)
	public String submitUpdate(@ModelAttribute("loja") Loja loja, BindingResult result, ModelMap model) {
		        
		if (result.hasErrors()) {
			return "error";
		}
		 
		lojaService.updateLoja(loja);
		return "redirect:/loja/index";
	}
	

	@RequestMapping(value = "/loja/index", method = RequestMethod.GET)
	public ModelAndView index() {
	        
	    ModelAndView mav = new ModelAndView("indexLoja");
	    mav.addObject("listaLojas", lojaService.getAllLoja());
	    return mav;
	 }

	@RequestMapping(value = "/loja/read", method = RequestMethod.GET)
	public ModelAndView read() {
	        
	    ModelAndView mav = new ModelAndView("readLoja");
	    mav.addObject("listaLojas", lojaService.getAllLoja());
	    return mav;
	}
}