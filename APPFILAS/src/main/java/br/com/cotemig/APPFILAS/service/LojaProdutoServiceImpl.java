package br.com.cotemig.APPFILAS.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.APPFILAS.model.Loja;
import br.com.cotemig.APPFILAS.model.LojaProduto;
import br.com.cotemig.APPFILAS.model.Produto;
import br.com.cotemig.APPFILAS.repository.LojaProdutoRepository;

@Service("lojaProdutoService")
public class LojaProdutoServiceImpl implements LojaProdutoService {

	@Autowired
	LojaProdutoRepository lojaProdutoRepository;
	
	public void insertListProdutoNaLoja(Loja loja, ArrayList<Produto> listProduto) {
		
		LojaProduto lojaProduto;
		
		for(Produto produto : listProduto) {
			
			lojaProduto = new LojaProduto();
			
			lojaProduto.setLoja(loja);
			lojaProduto.setProduto(produto);
			
			lojaProdutoRepository.save(lojaProduto);
		}
	}

	@Override
	public Optional<LojaProduto> getLojaProdutoById(Integer id) {
		return lojaProdutoRepository.findById(id);
	}

	@Override
	public List<LojaProduto> getAllLojaProduto() {
		return lojaProdutoRepository.findAll();
	}

	@Override
	public void deleteLojaProdutoById(Integer id) {
		lojaProdutoRepository.deleteById(id);
	}

	@Override
	public void deleteAllLojaProduto() {
		lojaProdutoRepository.deleteAll();
	}

	@Override
	public List<LojaProduto> buscarTodosProdutosDaLoja(Loja loja) {
		
		ArrayList<LojaProduto> listaLojaProduto = new ArrayList<>();
		
		listaLojaProduto.addAll(lojaProdutoRepository.buscarProdutosDaLoja(loja.getId()));
		
		return listaLojaProduto;
	}
}
