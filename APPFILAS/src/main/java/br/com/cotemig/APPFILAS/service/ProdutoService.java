package br.com.cotemig.APPFILAS.service;

import java.util.List;
import java.util.Optional;

import br.com.cotemig.APPFILAS.model.Produto;

public interface ProdutoService {

	Optional<Produto> getProdutoById(Integer id);
	List<Produto> getAllProduto();
	void deleteAllProduto();
	void deleteProdutoById(Integer id);
	void updateProdutoById(Integer id, Produto produto);
	void updateProduto(Produto produto);
	void insertProduto(Produto produto);
}
