package br.com.cotemig.APPFILAS.restController;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.cotemig.APPFILAS.model.Loja;
import br.com.cotemig.APPFILAS.service.LojaService;

@RestController
public class LojaCrudRestController extends BaseRestController {
		
	@Autowired
	private LojaService lojaService;
		
	@RequestMapping(value = "/teste/loja/post", method = RequestMethod.POST)
	public void post(String nome, String logradouro, String telefone, HttpServletResponse httpResponse) throws IOException {
		Loja loja = new Loja();
		loja.setNome(nome);
		loja.setLogradouro(logradouro);
		loja.setTelefone(telefone);
		getListLoja().add(loja);
		httpResponse.sendRedirect("/form");
	}
	
	@RequestMapping(value = "/rest/loja/getAll", method = RequestMethod.GET)
	public List<Loja> getAllLojas() {
		return lojaService.getAllLoja();
	}
	
	@RequestMapping(value = "/rest/loja/get/{id}", method = RequestMethod.GET)
	public Optional<Loja> getLojaById(@PathVariable("id") Integer id) {
		return lojaService.getLojaById(id);
	}
	
	@RequestMapping(value = "/rest/loja/deleteAll", method = RequestMethod.DELETE)
	public void deleteAllLojas() {
		lojaService.deleteAllLoja();
	}
	
	@RequestMapping(value = "/rest/loja/delete/{id}", method = RequestMethod.DELETE)
	public void deleteLojaById(@PathVariable("id") Integer id) {
		lojaService.deleteLojaById(id);
	}
	
	@RequestMapping(value = "/rest/loja/update/{id}", method = RequestMethod.POST)
	public void updateLojaById(@PathVariable("id") Integer id, Loja loja) {
		lojaService.updateLojaById(id, loja);
	}
	
	@RequestMapping(value = "/rest/loja/update", method = RequestMethod.POST)
	public void updateLoja(Loja loja) {
		lojaService.updateLoja(loja);
	}
}