package br.com.cotemig.APPFILAS.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.APPFILAS.model.Persona;
import br.com.cotemig.APPFILAS.repository.PersonaRepository;

@Service("personaService")
public class PersonaServiceImpl implements PersonaService{

	@Autowired
	PersonaRepository personaRepository;
	
	@Override
	public Optional<Persona> getPersonaById(Long id) {
		return personaRepository.findById(id);
	}

	@Override
	public List<Persona> getAllPersona() {
		return personaRepository.findAll();
	}

	@Override
	public void deleteAllPersona() {
		personaRepository.deleteAll();
	}

	@Override
	public void deletePersonaById(Long id) {
		personaRepository.deleteById(id);
	}

	@Override
	public void updatePersonaById(Long id, Persona persona) {
		Optional<Persona> optPersona = getPersonaById(id);
		optPersona.get().setNome(persona.getNome());
		optPersona.get().setCpf(persona.getCpf());
		 
		personaRepository.save(persona);
	}

	@Override
	public void updatePersona(Persona persona) {
		personaRepository.save(persona);
	}

	@Override
	public Persona insertPersona(Persona persona) {
		return personaRepository.save(persona);
	}

}
