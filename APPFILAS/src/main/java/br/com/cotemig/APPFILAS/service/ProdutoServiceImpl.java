package br.com.cotemig.APPFILAS.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.APPFILAS.model.Produto;
import br.com.cotemig.APPFILAS.repository.ProdutoRepository;

@Service("produtoService")
public class ProdutoServiceImpl implements ProdutoService {
	
	@Autowired
	ProdutoRepository produtoRepository;

	@Override
	public Optional<Produto> getProdutoById(Integer id) {
		return produtoRepository.findById(id);
	}

	@Override
	public List<Produto> getAllProduto() {
		return produtoRepository.findAll();
	}

	@Override
	public void deleteAllProduto() {
		produtoRepository.deleteAll();
	}

	@Override
	public void deleteProdutoById(Integer id) {
		produtoRepository.deleteById(id);		
	}

	@Override
	public void updateProdutoById(Integer id, Produto produto) {
		
		Optional<Produto> optProduto = getProdutoById(id);
		
		optProduto.get().setNome(produto.getNome());
		optProduto.get().setCategoria(produto.getCategoria());
		optProduto.get().setValor(produto.getValor());
		
		produtoRepository.save(produto);
	}

	@Override
	public void updateProduto(Produto produto) {
		produtoRepository.save(produto);
	}

	@Override
	public void insertProduto(Produto produto) {
		produtoRepository.save(produto);
	}

}
