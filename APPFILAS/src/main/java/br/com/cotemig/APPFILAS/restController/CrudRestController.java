package br.com.cotemig.APPFILAS.restController;

import java.util.List;
import java.util.Optional;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.cotemig.APPFILAS.model.Persona;
import br.com.cotemig.APPFILAS.service.PersonaService;

@RestController
@RequestMapping("/user")
@Api(value="USER CONTROLLER API")
public class CrudRestController extends BaseRestController {
	
	
	@Autowired
	private PersonaService personaService;


	@ApiOperation(value="Inserir usuário")
	@RequestMapping(value = "/teste/post", method = RequestMethod.POST)
	public Persona savePersona(Persona persona) {
	    return personaService.insertPersona(persona);
	}

	@ApiOperation(value="Buscar todos os usuários")
	@RequestMapping(value = "/rest/getAllPersonas", method = RequestMethod.GET)
	public List<Persona> getAllPersona() {
		return personaService.getAllPersona();
	}

	@ApiOperation(value="Buscar usuário por ID")
	@RequestMapping(value = "/rest/get/{id}", method = RequestMethod.GET)
	public Optional<Persona> getPersona(@PathVariable("id") Long id) {
		return personaService.getPersonaById(id);
	}

	@ApiOperation(value="Deletar todos os usuários")
	@RequestMapping(value = "/rest/deleteAll", method = RequestMethod.DELETE)
	public void deleteAllPersona() {
	    personaService.deleteAllPersona();
	}

	@ApiOperation(value="Deletar usuário por ID")
	@RequestMapping(value = "/rest/delete/{id}", method = RequestMethod.DELETE)
	public void deleteAluno(@PathVariable("id") Long id) {
		personaService.deletePersonaById(id);
	}

	@ApiOperation(value="Alterar usuário por ID")
	@RequestMapping(value = "/rest/update/{id}", method = RequestMethod.PUT)
	public void updatePersona(Persona persona, @PathVariable("id") Long id) {
	    personaService.updatePersonaById(id, persona);
	}

	@ApiOperation(value="Alterar todos os usuários")
	@RequestMapping(value = "/rest/insert", method = RequestMethod.POST)
	public void updatePersona(@RequestBody Persona persona) {
		personaService.insertPersona(persona);
	}
}
