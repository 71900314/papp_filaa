package br.com.cotemig.APPFILAS.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.APPFILAS.model.Loja;
import br.com.cotemig.APPFILAS.model.Produto;
import br.com.cotemig.APPFILAS.restController.BaseRestController;
import br.com.cotemig.APPFILAS.service.*;

@Controller
public class LojaProdutoController extends BaseRestController {

	@Autowired 
	private LojaProdutoService lojaProdutoService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private LojaService lojaService;

	@RequestMapping(value = "/lojaproduto/insert", method = RequestMethod.GET)
	public ModelAndView insert(Integer id) {

		ModelAndView mav = new ModelAndView("lojaProduto");
		mav.addObject("loja", lojaService.getLojaById(id).get());
		mav.addObject("listProdutos", produtoService.getAllProduto());
		return mav;
	}
	
	@RequestMapping(value = "/lojaproduto/insert", method = RequestMethod.POST)
	public String submitInsert(@ModelAttribute("loja") Loja loja, @ModelAttribute("listProduto") ArrayList<Produto> listProduto, BindingResult result, ModelMap model) {
		
		if (result.hasErrors()) {
			return "error";
		}
		
		lojaProdutoService.insertListProdutoNaLoja(getLojaEx(), (ArrayList<Produto>) getListaProduto());
		
		return "redirect:loja/index"; 
	}	

	@RequestMapping(value = "/lojaproduto/insertListProduto", method = RequestMethod.GET)
	public String insertListaProduto(Integer id, boolean add) {

		Produto produtoEx = new Produto();
		
		for (Produto prod : getListaProdutoEx()) {
			if (prod.getId().equals(id)) {
				produtoEx = prod;
				break;
			}
		}
		
		if (add) {
			getListaProduto().add(produtoEx);
			produtoEx.setAdd(false);
		} else { 
			getListaProduto().remove(produtoEx);
			produtoEx.setAdd(true);
		}
	    
	    return "redirect:loja/index";
	}
	
	@RequestMapping(value = "/personaproduto/buscarprodutos", method = RequestMethod.GET)
	public ModelAndView buscarProdutos(Integer id) {
		
		Loja loja = lojaService.getLojaById(id).get();
		
		ModelAndView mav = new ModelAndView("updateLojaProduto");
		mav.addObject("loja", loja);
	    mav.addObject("listLojaProdutos", lojaProdutoService.buscarTodosProdutosDaLoja(loja));
		
		return mav;
	}
}