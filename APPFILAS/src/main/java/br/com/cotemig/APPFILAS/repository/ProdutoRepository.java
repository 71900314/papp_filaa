package br.com.cotemig.APPFILAS.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.cotemig.APPFILAS.model.Produto;

@Repository("produtoRepository")
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
