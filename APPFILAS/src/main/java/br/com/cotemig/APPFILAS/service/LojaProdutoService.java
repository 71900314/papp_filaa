package br.com.cotemig.APPFILAS.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.cotemig.APPFILAS.model.Loja;
import br.com.cotemig.APPFILAS.model.LojaProduto;
import br.com.cotemig.APPFILAS.model.Produto;

public interface LojaProdutoService {

	Optional<LojaProduto> getLojaProdutoById(Integer id);
	List<LojaProduto> getAllLojaProduto();
	void deleteLojaProdutoById(Integer id);
	void deleteAllLojaProduto();
	void insertListProdutoNaLoja(Loja loja, ArrayList<Produto> listProduto);
	List<LojaProduto> buscarTodosProdutosDaLoja(Loja loja);
}
