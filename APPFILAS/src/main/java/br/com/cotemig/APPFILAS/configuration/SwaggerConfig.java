package br.com.cotemig.APPFILAS.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    protected static final String TITLE = "PAPP FILAS";
    protected static final String DESCRIPTION = "API REST PARA COMPRA DE PRODUTOS";
    protected static final String VERSION = "1.0";
    protected static final String LICENSE = "Apache License Version 2.0";
    protected static final String LICENSE_URL = "https://www.apache.org/licesen.html";
    protected static final String CONTACT_NAME = "GRUPO TRABALHO SOPHIA";
    protected static final String URL = "https://gitlab.com/71900314/papp_fila";
    protected static final String EMAIL = "email@email.com";

    @Bean
    public Docket productApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.cotemig.pAPP_Filas"))
                .paths(regex("/user.*"))
                .build()
                .apiInfo(metaInfo());
    }

    private ApiInfo metaInfo() {

        return new ApiInfoBuilder().title(TITLE)
                .description(DESCRIPTION)
                .version(VERSION)
                .contact(new Contact(CONTACT_NAME, URL, EMAIL))
                .license(LICENSE)
                .licenseUrl(LICENSE_URL)
                .build();
    }
}
