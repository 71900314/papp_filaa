package br.com.cotemig.APPFILAS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.cotemig.APPFILAS.model.LojaProduto;

@Repository("lojaProdutoRepository")
public interface LojaProdutoRepository extends JpaRepository<LojaProduto, Integer> {

	@Query(value = "SELECT * FROM LOJA_PRODUTO WHERE FK_LOJA = ?1", nativeQuery = true)
	List<LojaProduto> buscarProdutosDaLoja(Integer id);
}
