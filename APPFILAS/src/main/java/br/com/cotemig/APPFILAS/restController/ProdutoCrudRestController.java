package br.com.cotemig.APPFILAS.restController;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.cotemig.APPFILAS.model.Produto;
import br.com.cotemig.APPFILAS.service.ProdutoService;

@RestController
public class ProdutoCrudRestController extends BaseRestController {
	
	@Autowired
	private ProdutoService produtoService;
	
	@RequestMapping(value = "/teste/produto/post", method = RequestMethod.POST)
	public void post(String nome, String categoria, Float valor, HttpServletResponse httpResponse) throws IOException {
	    Produto produto = new Produto();
	    produto.setNome(nome);
	    produto.setCategoria(categoria);
	    produto.setValor(valor);
		getListaProduto().add(produto);
		httpResponse.sendRedirect("/form");
	}
	
	@RequestMapping(value = "/rest/produto/getAll", method = RequestMethod.GET)
	public List<Produto> getAllProdutos() {
		return produtoService.getAllProduto();	
	}
	
	@RequestMapping(value = "/rest/produto/get/{id}", method = RequestMethod.GET)
	public Optional<Produto> getProdutoById(@PathVariable("id") Integer id) {
		return produtoService.getProdutoById(id);
	}
	
	@RequestMapping(value = "/rest/produto/deleteAll", method = RequestMethod.DELETE)
	public void deleteAllProdutos() {
		produtoService.deleteAllProduto();
	}
	
	@RequestMapping(value = "/rest/produto/delete/{id}", method = RequestMethod.DELETE)
	public void deleteProdutoById(@PathVariable("id") Integer id) {
		produtoService.deleteProdutoById(id);
	}
	
	@RequestMapping(value = "/rest/produto/update/{id}", method = RequestMethod.POST)
	public void updateProdutoById(@PathVariable("id") Integer id, Produto produto) {
		produtoService.updateProdutoById(id, produto);
	}
	
	@RequestMapping(value = "/rest/produto/update", method = RequestMethod.POST)
	public void updateProduto(Produto produto) {
		produtoService.updateProduto(produto);
	}
}
