package br.com.cotemig.APPFILAS.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.cotemig.APPFILAS.model.PersonaProduto;
import br.com.cotemig.APPFILAS.model.Persona;
import br.com.cotemig.APPFILAS.model.Produto;

public interface PersonaProdutoService {
	
	Optional<PersonaProduto> getPersonaProdutoById(Long id);
	List<PersonaProduto> getAllPersonaProduto();
	void deleteAllPersonaProduto();
	void deletePersonaProdutoById(Long id);
	void insertListaProdutoNoPersona(Persona persona, ArrayList<Produto> listaProduto);
	List<PersonaProduto> buscarTodosProdutosDoPersona(Persona persona);
}
