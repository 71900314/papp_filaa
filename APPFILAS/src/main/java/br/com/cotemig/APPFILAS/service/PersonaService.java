package br.com.cotemig.APPFILAS.service;

import java.util.List;
import java.util.Optional;

import br.com.cotemig.APPFILAS.model.Persona;


public interface PersonaService {
	
	Optional<Persona> getPersonaById(Long id);
	List<Persona> getAllPersona();
	void deleteAllPersona();
	void deletePersonaById(Long id);
	void updatePersonaById(Long id, Persona persona);
	void updatePersona(Persona persona);
	Persona insertPersona(Persona persona);

}
