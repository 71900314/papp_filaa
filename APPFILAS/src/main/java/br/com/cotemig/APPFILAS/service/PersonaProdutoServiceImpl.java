package br.com.cotemig.APPFILAS.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.APPFILAS.model.Persona;
import br.com.cotemig.APPFILAS.model.PersonaProduto;
import br.com.cotemig.APPFILAS.model.Produto;
import br.com.cotemig.APPFILAS.repository.PersonaProdutoRepository;

@Service("personaProdutoService")
public class PersonaProdutoServiceImpl implements PersonaProdutoService{

	@Autowired
	PersonaProdutoRepository personaProdutoRepository;

	@Override
	public Optional<PersonaProduto> getPersonaProdutoById(Long id) {
		return personaProdutoRepository.findById(id);
	}

	@Override
	public List<PersonaProduto> getAllPersonaProduto() {
		return personaProdutoRepository.findAll();
	}

	@Override
	public void deleteAllPersonaProduto() {
		personaProdutoRepository.deleteAll();
	}

	@Override
	public void deletePersonaProdutoById(Long id) {
		personaProdutoRepository.deleteById(id);
	}

	@Override
	public void insertListaProdutoNoPersona(Persona persona, ArrayList<Produto> listaProduto) {
		PersonaProduto personaProdutoEx;
		
		for(Produto prodEx : listaProduto) {
			
			personaProdutoEx = new PersonaProduto();
			
			personaProdutoEx.setPersona(persona);
			personaProdutoEx.setProduto(prodEx);
			
			personaProdutoRepository.save(personaProdutoEx);
			
		}
	}

	@Override
	public List<PersonaProduto> buscarTodosProdutosDoPersona(Persona persona) {
		
		ArrayList<PersonaProduto> listaPersonaProduto = new ArrayList<>();
		
		listaPersonaProduto.addAll(personaProdutoRepository.buscarProdutosDoPersona(persona.getId()));
		
		return listaPersonaProduto;
	}


}
